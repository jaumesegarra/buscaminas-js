export class gameTime {
	constructor (){
		this._$el = document.getElementById("timer").getElementsByTagName("b");
		this._time = 0;
		this._interval = null;	

		this._implement();
	}

	get current () {
		return this._time;
	}

	_implement (){
		this._$el[0].innerHTML = '00';
		this._$el[1].innerHTML = '00';
	}

	start (){
		this._interval = this._intervalTrigger();
	}

	stop (){
		window.clearInterval(this._interval);
	}

	_intervalTrigger() {
		return window.setInterval( function() {
			this._time++;

			this._$el[0].innerHTML = ("0" + parseInt(this._time/60)).slice(-2);
			this._$el[1].innerHTML = ("0" + parseInt(this._time%60)).slice(-2);

		}.bind(this), 1000 );
	}
}