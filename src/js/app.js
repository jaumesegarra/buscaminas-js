import {modal} from './modal.js';
import {game} from './game.js';
import {clasificacion} from './clasificacion.js';
import _ from 'lodash';

let current_game;

function implement() {
	/*Eventos de los botones del modal para cancelar la partida en curso*/

	var $confirmarVolverSi = document.querySelector("#confirm_volverModal #yes");
	$confirmarVolverSi.addEventListener("click", () => current_game.leave(), false); // Volver al menú principal

	var $confirmarVolverNo = document.querySelector("#confirm_volverModal #no");
	$confirmarVolverNo.addEventListener("click", () => modal.cerrar(), false); // Cerrar el modal para continuar la partida

	var $jugarNivelBtns = document.getElementsByClassName("jugarNivelButton");
	_.forEach($jugarNivelBtns, ($jugarNivelBtn)=>{
		$jugarNivelBtn.addEventListener("click", (e)=> {
			var $this = e.target;
			var nivel = parseInt($this.getAttribute("data-nivel"));

			var tableroX, tableroY, bombas;
			switch (nivel) {
				case 1:
				tableroX = 16;
				tableroY = 16;
				bombas = 40;
				break;
				case 2:
				tableroX = 16;
				tableroY = 30;
				bombas = 99;
				break;
				default:
				tableroX = 8;
				tableroY = 8;
				bombas = 10;
				break;
			}

			current_game = new game(tableroX, tableroY, bombas, nivel);
		}, false);
	});

	var $jugarNivelCustomBtn = document.getElementById("jugarNivelCustomButton");
	$jugarNivelCustomBtn.addEventListener("click", () => modal.mostrar_dada("partidaPersonalizadaModal"), false);

	function e_modalPersonalizadaForm(e) {
		e.preventDefault();

		var $x = document.getElementById("pp_x");
		var $y = document.getElementById("pp_y");
		var $bmb = document.getElementById("pp_bmb");

		var x = parseInt($x.value);
		var y = parseInt($y.value);
		var bmb = parseInt($bmb.value);

		var continuar = true;

		if(x<8 || x>24){
			$x.value="";
			continuar=false;
		}

		if(y<8 || y>32){
			$y.value="";
			continuar=false;
		}

		if(bmb<1 || (x*y)/3 <= bmb){
			$bmb.value="";
			continuar=false;
		}

		if(continuar){
			modal.cerrar();
			current_game = new game(x, y, bmb, 3);
		}
		e.target.reset();
	}

	var $jugarNivelCustomForm = document.getElementById("partida_personalizadaForm");
	$jugarNivelCustomForm.addEventListener("submit", e_modalPersonalizadaForm, false);

	function e_modalPersonalizadaGenerarNumBombas(e) {
		var $x = document.getElementById("pp_x");
		var $y = document.getElementById("pp_y");
		var $bmb = document.getElementById("pp_bmb");

		if(($x.value.trim() != "" && $x.value>=8 && $x.value<=24) && ($y.value.trim() != "" && $y.value>=8 && $y.value<=32)){
			$bmb.setAttribute("placeholder", "1 a "+parseInt(($x.value*$y.value)/3));
		}else {
			$bmb.setAttribute("placeholder", "1 a 1/3");
		}
	}

	var $jugarNivelCustomInputX = document.getElementById("pp_x");
	$jugarNivelCustomInputX.addEventListener("keyup",e_modalPersonalizadaGenerarNumBombas , false);

	var $jugarNivelCustomInputY = document.getElementById("pp_y");
	$jugarNivelCustomInputY.addEventListener("keyup",e_modalPersonalizadaGenerarNumBombas , false);

	var $salirPartidaBtn = document.getElementById("salirPartida");
	$salirPartidaBtn.addEventListener("click", () => {
		if(current_game.isActiva)
			modal.mostrar_dada("confirm_volverModal");
		else
			current_game.leave();

	}, false);

	var $reiniciarPartidaBtn = document.getElementById("reiniciarPartida");
	$reiniciarPartidaBtn.addEventListener("click", () => current_game = current_game.restart(), false);

	document.getElementById("pgge_nombre").setAttribute("placeholder", ((localStorage.getItem("usuario")) ? localStorage.getItem("usuario") : "Usuario1"));

	var $guardarEstadisticasPGForm = document.getElementById("partida_ganada_guardarForm");
	$guardarEstadisticasPGForm.addEventListener("submit", (e) => {
		e.preventDefault();
		clasificacion.savePartida(current_game);
	}, false);
}

modal.implement();
implement();
clasificacion.refrescarDiv();