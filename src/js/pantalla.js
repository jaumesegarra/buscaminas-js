import _ from 'lodash';

export class pantalla{
	/**
		Mostrar la pantalla dada
		**/
		static mostrar(p){
		var $pantallasDiv = document.getElementsByClassName("pantallas")[0]; // Contenedor con todas la pantallas del juego
		var $sections = $pantallasDiv.getElementsByTagName("section"); // Array con las pantallas

		_.forEach($sections, ($section) => { // Recorre las pantallas
			$section.style.display = 'none'; // y las oculta
		});			

		document.getElementById(p).style.display = 'block'; // Muestra la pantalla dada
	}
}