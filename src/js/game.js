import {pantalla} from './pantalla.js';
import {modal} from './modal.js';

import {gameTime} from './game_time.js';
import {gameTablero} from './game_tablero.js';
import _ from 'lodash';

function emoticonState(state, text) {
	var $emoji = document.getElementById("emoji_partida");
	var $emojiText = document.getElementById("emojiText_partida");

	$emoji.className=state;
	$emojiText.innerHTML=text;
}

function evlistener_detectEscape(e){
	if (e.keyCode == 27)
		modal.mostrar_dada("confirm_volverModal");
}

export class game {
	constructor (tx, ty, bmb, nivel){
		this._tx=tx;
		this._ty=ty;
		this._bmb = bmb;
		this._nivel = nivel;

		this._tablero = new gameTablero(this);
		this._finalizada = false;
		this._timing = new gameTime();

		this._implement();
	}

	get tableroX () {
		return this._tx;
	}

	get tableroY () {
		return this._ty;
	}

	get bombasN () {
		return this._bmb;
	}

	get getMarcadasN (){
		return this._tablero._marcadas.length;
	}

	get tiempo () {
		return this._timing.current;
	}

	get isActiva(){
		return (!this._finalizada && !this._tablero.isEmpty);
	}

	get isFinished (){
		return this._finalizada;
	}

	updateMarcadasEl (){
		var $num_minas = document.getElementById("num_minas").getElementsByTagName("b")[0];
		$num_minas.innerHTML = this._bmb - this._tablero._marcadas.length;
	}

	_implement(){
		document.addEventListener('keypress', (e) => evlistener_detectEscape(e), false); // Evento que se ejecutará al introducir cualquier tecla en el body. Para detectar la tecla ESC y mostrar el modal para salir de la partida 
		
		this.updateMarcadasEl();

		emoticonState("happy", "Empieza la partida!");
		pantalla.mostrar("juego");
	}

	start(){
		this._timing.start();
		emoticonState("happy", "Sorprendeme!");
	}

	finalize(res) {
		this._finalizada = true;
		this._timing.stop();

		if(!res){
			this._tablero.explotar();
			emoticonState("sad", "Perdiste!");
		}else{
			this._tablero.destapar();
			emoticonState("lol", "Ganaste!");

			setTimeout(()=> modal.mostrar_dada("guardar_estadisticasModal"), 600);
		}
	}

	leave(){
		document.removeEventListener('keypress', evlistener_detectEscape); // Elimina el evento para salir de la partida al introducir la tecla ESC
		this._timing.stop();

		modal.cerrar();
		pantalla.mostrar("principal");
	}

	restart(){
		this._timing.stop();
		return new game(this._tx, this._ty, this._bmb,this._nivel);
	}
}