import {modal} from './modal.js';
import _ from 'lodash';

export class clasificacion {
	static refrescarDiv(){
		var $partidasGanadasDiv = document.getElementById("partidas_ganadas");
		$partidasGanadasDiv.innerHTML = "";

		if(localStorage.getItem("partidas")){
			var partidas = JSON.parse(localStorage.getItem('partidas'));
			var $ul = document.createElement("ul");

			for(var i=0;i<partidas.length && i<10;i++){
				var p = partidas[i];
				var $li = document.createElement("li");

				var $usuarioDiv = document.createElement("div");
				$usuarioDiv.className = "usuario";
				$usuarioDiv.innerHTML= p.usuario;
				$li.appendChild($usuarioDiv);

				var $dificultadDiv = document.createElement("div");
				$dificultadDiv.className = "nivel";
				switch(p.nivel){
					case 0: 
					$dificultadDiv.innerHTML = "Principiante";
					break;
					case 1: 
					$dificultadDiv.innerHTML = "Intermedio";
					break;
					case 2: 
					$dificultadDiv.innerHTML = "Experto";
					break;
					case 3:
					$dificultadDiv.innerHTML = `Personalizado (${p.nivel_data.x}x${p.nivel_data.y} [${p.nivel_data.bombas} bombas])`;
					break;
				}
				$li.appendChild($dificultadDiv);
				var $tiempoDiv = document.createElement("div");

				$tiempoDiv.innerHTML= ("0" + parseInt(p.tiempo/60)).slice(-2)+':'+("0" + parseInt(p.tiempo%60)).slice(-2);
				$li.appendChild($tiempoDiv);

				$ul.appendChild($li);
			}

			$partidasGanadasDiv.appendChild($ul);
		}else{
			var $noresultados = document.createElement("p");
			$noresultados.innerHTML="Ninguna partida ganada hasta el momento.";
			$partidasGanadasDiv.appendChild($noresultados);
		}
	}

	static savePartida(partida){
		var usuarioTxt = document.getElementById("pgge_nombre").value;
		var usuario = ((usuarioTxt.trim()) ? usuarioTxt : localStorage.getItem("usuario"));

		var datos = {
			'usuario': usuario,
			'nivel': partida.dificultad,
			'nivel_data': {
				'x': partida.tableroX,
				'y': partida.tableroY,
				'bombas': partida.bombasN
			},
			'tiempo': partida.tiempo
		};

		var partidas_guardadas = (localStorage.getItem('partidas')) ? JSON.parse(localStorage.getItem('partidas')) : [];
		partidas_guardadas.push(datos);

		partidas_guardadas = _.sortBy(partidas_guardadas, [(o)=>{
			return (o.nivel_data.x*o.nivel_data.y+o.nivel_data.bombas-o.tiempo)*-1;
		}]);

		if(partidas_guardadas.length > 10)
			partidas_guardadas = _.initial(partidas_guardadas);

		localStorage.setItem('partidas', JSON.stringify(partidas_guardadas));

		localStorage.setItem("usuario", usuario);
		document.getElementById("pgge_nombre").setAttribute("placeholder", usuario);
		
		this.refrescarDiv();
		modal.cerrar();
	}
}