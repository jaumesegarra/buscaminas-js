import _ from 'lodash';

export class gameCasilla {
	constructor (x, y, $col, _this){
		this._tablero = _this;
		this._game = _this.getGame;

		this._value = 0;
		this._x = x;
		this._y = y;
		this._$el = undefined;

		this._implement($col);
	}

	get isBomb (){
		return (this._value == 9);
	}

	get getX (){
		return this._x;
	}

	get getY (){
		return this._y;
	}

	get getValue (){
		return this._value;
	}

	set setValue(value){
		this._value = value;
	}

	get getElement (){
		return this._$el;
	}

	_implement($col){
		this._$el = document.createElement("td");
		this._$el.addEventListener("click", (e) => this._eClick(e),false);
		this._$el.addEventListener("contextmenu", (e) => this._eClickDerecho(e),false);

		$col.appendChild(this._$el);
	}

	_eClick(e){
		var $this = this._$el;

		this._tablero.generarBombas(this);

		if(!this._game.isFinished && !$this.classList.contains("marcada") && !$this.classList.contains("dudable")){
			if(this._tablero.getElement[this._x][this._y].getValue == 9){
				this._game.finalize(false);
				$this.classList.add("red");
			}
			else
				this._pintar();
		}
	}

	_eClickDerecho(e) {
		e.preventDefault();
		var $this = this._$el;

		if(!this._tablero.getGame.isFinished && !this._tablero.isEmpty && !$this.classList.contains("destapada")){
			if(!$this.classList.contains("marcada") && !$this.classList.contains("dudable")){
				$this.classList.add("marcada");
				this._tablero.pushMarcada(this);
			}
			else{	
				if($this.classList.contains("dudable"))
					$this.classList.remove("dudable");

				if($this.classList.contains("marcada")){
					this._tablero.removeMarcada(this);
					$this.className = "dudable";		
				}
			}
		}
	}

	obtenerAlrededores(){
		var alrededores = [];

		for(let tm_x = this._x-1; tm_x<=this._x+1;tm_x++){
			for(let tm_y= this._y-1; tm_y<=this._y+1;tm_y++){
				if(this._tablero._el[tm_x] !== undefined && this._tablero._el[tm_x][tm_y] !== undefined){
					alrededores.push(this._tablero.getElement[tm_x][tm_y]);
				}
			}
		}
		
		return alrededores;
	}

	_propagar() {
		var alrededores = this.obtenerAlrededores();
		alrededores.forEach( function(element) {
			element._pintar();
		}.bind(this));
	}

	_pintar() {
		var $casilla = this._$el;

		if(!$casilla.classList.contains("destapada")){
			this._tablero.destapadas++;

			if(this.getValue != 0){
				$casilla.innerHTML = `<font> ${this.getValue} </font>`;
				$casilla.classList.add(((this.getValue < 3 ) ? "bombas_"+this.getValue : "bombas_3"));
			}

			$casilla.classList.add("destapada");

			if(this.getValue == 0)
				this._propagar();

			this._tablero.comprobarTodasCasillasDestapadas();
		}
	}
}