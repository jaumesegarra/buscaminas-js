import _ from 'lodash';
import {gameCasilla} from './game_casilla.js';

export class gameTablero {
	constructor (_this){
		this._game = _this;

		this._el = [];
		this._bombas = [];
		this._marcadas = [];

		this._is_vacio = true; 
		this._destapadas = 0;

		this._implement();
	}

	get getElement(){
		return this._el;
	}

	get getGame (){
		return this._game;
	}

	get isEmpty (){
		return this._is_vacio;
	}

	get isDestapado (){
		return (this._destapadas == (this._game.tableroX*this._game.tableroY)-this._game.bombasN);
	}

	get destapadas (){
		return this._destapadas;
	}

	set destapadas(des){
		this._destapadas=des;
	}

	pushMarcada(cas){
		this._marcadas.push(cas);
		this._game.updateMarcadasEl();
	}

	removeMarcada(cas){
		let index = this._marcadas.indexOf(cas);
		if (index > -1)
			this._marcadas.splice(index, 1);
		this._game.updateMarcadasEl();
	}

	_implement (){
		document.getElementById("tablero").innerHTML = "";

		_.times(this._game.tableroX, (ix)=>{
			var filas = [];
			var $columna = document.createElement("tr");

			_.times(this._game.tableroY, (iy)=>{
				filas[iy] = new gameCasilla(ix, iy, $columna, this);
			});

			this._el[ix] = filas;

			document.getElementById("tablero").appendChild($columna);
		});
	}

	generarBombas(casilla1){
		if(this._is_vacio){

			// Poner bombas
			_.times(this._game.bombasN, ()=>{
				var asignada = false;

				while(!asignada){
					var x = parseInt(Math.random() * this._game.tableroX);
					var y = parseInt(Math.random() * this._game.tableroY);

					if(!this._el[x][y].isBomb && (casilla1.getX != x && casilla1.getY != y)){
						this._el[x][y].setValue = 9;
						this._bombas.push(this._el[x][y]);
						asignada = true;
					}
				}
			});

			// Poner numeros de alrededor
			_.times(this._bombas.length, (j)=>{
				var alrededores = this._bombas[j].obtenerAlrededores();
				_.times(alrededores.length, (i)=>{
					var el = alrededores[i];
					if(!el.isBomb)
						el.setValue = el.getValue + 1; 
				})
			});

			this._is_vacio = false;
			this._game.start();
		}
	}
	
	comprobarTodasCasillasDestapadas(){
		if(this.isDestapado)
			this._game.finalize(true);
	}

	destapar() {
		_.forEach(this._bombas, (bomba)=>{
			var $casilla = bomba.getElement;
			$casilla.className = "marcada";
		});
	}

	explotar(){
		_.forEach(this._bombas, (bomba)=>{
			var $casilla = bomba.getElement;
			if(!$casilla.classList.contains("marcada"))
				$casilla.classList.add("bomba");
		});

		_.forEach(this._marcadas, (marcada)=>{
			if(!marcada.isBomb)
				marcada.getElement.className="no_bomba";
		});
	}
}