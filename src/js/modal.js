import _ from 'lodash';

export class modal{
	static implement (){
		var $modals = document.getElementsByClassName("js_abrir_modal");
		_.forEach($modals, ($modal)=>{
			$modal.addEventListener("click", this._mostrar, false);
		});

		/*
			Evento para cerrar los modales al hacer click en la 'x' del modal
		*/
		var $modals_close = document.getElementsByClassName("close_modal");
		_.forEach($modals_close, ($modal_close)=>{
			$modal_close.addEventListener("click", this.cerrar, false)
		});
	}

	/**
		Función que se ejecutará al hacer click en un enlace para abrir un modal y que mostrará el modal cuyo "id" sea el que tenga el elemento en el atributo "data-id".
	**/
	static _mostrar(){
		modal.mostrar_dada(this.getAttribute("data-id"));
	}

	/**
	Función que mostrará el modal con el "id" dado
	**/
	static mostrar_dada(id){
		var $modalDiv = document.getElementById(id); // Obtener modal
		var $modalBackgroundDiv = document.getElementById("alerts_background"); // Obtener fondo común de los modales

		/*
			Mostrar ambos elementos
		*/
		$modalDiv.style.display = 'block';
		$modalBackgroundDiv.style.display = 'block';


		if($modalDiv.classList.contains('closeable')) // Si el modal tiene la clase "closeable" (se permite cerrarlo)
			document.getElementsByClassName("pantallas")[0].addEventListener("click", this._evlistener_modal_close, false); // Añadir evento para ocultar el modal al hacer click en el div de las pantallas del juego 
		
	}

	/**
		Función que se ejecuta al hacer click en el body (cuando hay un modal abierto).
	**/
	static _evlistener_modal_close(e){
		if(e.target.id == 'alerts_background') // Si se ha hecho click fuera del modal
			modal.cerrar(); // lo cierra
	}

	/**
		Función para cerrar el modal activo
	**/
	static cerrar(){
		var $modals = document.getElementsByClassName("alert"); // Obtener modales del juego
		var $modalBackgroundDiv = document.getElementById("alerts_background"); // Obtener fondo de los modales común

		_.forEach($modals, ($modal) => { // Recorrer modales
			$modal.style.display = 'none'; // ocultarlos si no lo están
		});			

		$modalBackgroundDiv.style.display = 'none'; // Ocultar fondo común

		document.getElementsByClassName("pantallas")[0].removeEventListener('click', this._evlistener_modal_close); // Eliminar evento para ocultar el modal al hacer click en el div de las pantallas del juego 
	}
}